import React, { useState, useEffect, useRef } from "react";
import KeyPadComponent from "./components/KeyPadComponent";
import "./App.css";


function App() {
 
  const [result, setResult] = useState("");
  const inputRef = useRef(null);

  useEffect(() => inputRef.current.focus());

  function backspace() {
    setResult(result.slice(0, -1));
  }

  function clear() {
    setResult("");
  }

  function calculate() {
    try {
   
      setResult(eval(result).toString());
    } catch (error) {
      setResult("Error");
    }
  }

  function onClick(e) {
    if(e=== 'result'){
      calculate()
    }
    else if(e==='clear'){
      clear()
    }
    else if(e === 'backspace'){
      backspace()
    }

    else{
    setResult(result.concat(e));
    }
  }


  return (
    <div>
        <div className="calc-app">
            <h1>Simple Calculator</h1>
            <from>
              <input type="text" value={result} ref={inputRef} />
            </from>
            <KeyPadComponent onClick={onClick}/>
        </div>
    </div>
);
}

export default App;